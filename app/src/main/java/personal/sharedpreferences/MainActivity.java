package personal.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText editTextIn;
    private Button buttonSave;
    private TextView textView;
    private final static String SHAREDPREFERENCES_FOLDER_NAME = "Test_SP";
    private final static String SHAREDPREFERENCES_VALUE_KEY = "text_key";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }




    private void initComponents() {
        this.editTextIn = (EditText) this.findViewById(R.id.edit_text_in);
        this.buttonSave = (Button) this.findViewById(R.id.button_save);
        this.textView = (TextView) this.findViewById(R.id.text_view_out);

        final SharedPreferences sharedPreferences = this.getSharedPreferences(MainActivity.SHAREDPREFERENCES_FOLDER_NAME, Context.MODE_PRIVATE);
        String text = sharedPreferences.getString(MainActivity.SHAREDPREFERENCES_VALUE_KEY, "Enter Text!");
        this.textView.setText(text);

        this.buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                String text = MainActivity.this.editTextIn.getText().toString();
                text = (text==null) ? "" : text;
                editor.putString(MainActivity.SHAREDPREFERENCES_VALUE_KEY,text);
                editor.commit();
                MainActivity.this.textView.setText(text);
            }
        });
    }

}
